package uz.yuyar.appwarehouse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.yuyar.appwarehouse.entity.template.AbstractEntity;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Client extends AbstractEntity {
    @Column(unique = true, nullable = false)
    private String phoneNumber;

}
