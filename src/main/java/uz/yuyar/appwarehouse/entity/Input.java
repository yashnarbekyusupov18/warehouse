package uz.yuyar.appwarehouse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Input {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true,nullable = false)
    private String code;
    @Column(nullable = false)
    private String facture_number;
    private Timestamp date;

    @ManyToOne
    private Warehouse warehouse_id;

    @ManyToOne
    private Supplier supplier_id;
    @ManyToOne
    private Currency currency_id;


}
