package uz.yuyar.appwarehouse.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.yuyar.appwarehouse.entity.template.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Warehouse extends AbstractEntity {


}
