package uz.yuyar.appwarehouse.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.yuyar.appwarehouse.entity.template.AbstractEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Currency extends AbstractEntity {

}
