package uz.yuyar.appwarehouse.entity;

import lombok.Data;
import uz.yuyar.appwarehouse.entity.template.AbstractEntity;

import javax.persistence.Entity;

@Data
@Entity
public class Measurement extends AbstractEntity {

}
