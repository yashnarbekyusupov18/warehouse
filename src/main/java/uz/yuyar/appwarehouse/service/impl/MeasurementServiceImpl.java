package uz.yuyar.appwarehouse.service.impl;

import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.yuyar.appwarehouse.dto.requests.AddMeasurementRequest;
import uz.yuyar.appwarehouse.dto.requests.EditMeasurementRequest;
import uz.yuyar.appwarehouse.entity.Measurement;
import uz.yuyar.appwarehouse.repository.MeasurementRepository;
import uz.yuyar.appwarehouse.service.MeasurementService;

import java.util.Optional;

@AllArgsConstructor
@Service
public class MeasurementServiceImpl implements MeasurementService {

    private final MeasurementRepository repository;

    @Override
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(repository.findAll());
    }

    @Override
    public ResponseEntity<?> getById(Long id) {
        Optional<Measurement> optional = repository.findById(id);
        if (optional.isPresent())
            return ResponseEntity.ok(repository.findById(id));
        else return ResponseEntity.badRequest().body("Measurement not found");
    }

    @Override
    public ResponseEntity<?> addMeasurement(AddMeasurementRequest request) {
        boolean existsByName = repository.existsByName(request.getName());
        if (existsByName == false) {
            Measurement measurement = new Measurement();
            measurement.setName(request.getName());
            measurement.setActive(request.isActive());
            return ResponseEntity.ok(repository.save(measurement));
        } else {
            return ResponseEntity.badRequest().body("Not added. This measurement name allready exist");
        }
    }

    @Override
    public ResponseEntity<?> editMeasurement(Long id, EditMeasurementRequest request) {
        Optional<Measurement> optional = repository.findById(id);
        if (optional.isPresent()) {
            if(repository.existsByName(request.getName())){
                return ResponseEntity.ok("Not edited. This measurement name allready exist");
            }
            Measurement measurement = new Measurement();
            measurement = optional.get();
            measurement.setName(request.getName());
            measurement.setActive(request.isActive());
            repository.save(measurement);
            return ResponseEntity.ok("Successfully edited");
        }
        return ResponseEntity.badRequest().body("Measurement not found");
    }

    @Override
    public ResponseEntity<?> deleteMeasurement(Long id) {
        Optional<Measurement> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok("name: " + optional.get().getName() + " successfully deleted");
        }
        else return ResponseEntity.badRequest().body("Not deleted. Measurement not found");
    }
}
