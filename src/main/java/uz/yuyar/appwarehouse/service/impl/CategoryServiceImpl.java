package uz.yuyar.appwarehouse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.yuyar.appwarehouse.dto.CategoryDto;
import uz.yuyar.appwarehouse.dto.requests.AddCategoryRequest;
import uz.yuyar.appwarehouse.dto.requests.EditCategoryRequest;
import uz.yuyar.appwarehouse.entity.Category;
import uz.yuyar.appwarehouse.entity.Measurement;
import uz.yuyar.appwarehouse.repository.CategoryRepository;
import uz.yuyar.appwarehouse.service.CategoryService;

import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryRepository repository;

    @Override
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(repository.findAll());
    }

    @Override
    public ResponseEntity<?> getById(Long id) {
        Optional<Category> optional = repository.findById(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok(optional.get());
        } else return ResponseEntity.badRequest().body("Category not found");
    }

    @Override
    public ResponseEntity<?> addCategory(CategoryDto request) {
        boolean existsByName = repository.existsByName(request.getName());
        if(existsByName == false){
            Category category = new Category();
            category.setName(request.getName());
            if(request.getParentCategoryId() != null){
                Optional<Category> optionalParentCategory = repository.findById(request.getParentCategoryId());
                if(!optionalParentCategory.isPresent()){
                    return ResponseEntity.badRequest().body("Bunday categoriya mavjud emas");
                }
                else category.setParentCategory(optionalParentCategory.get());
            }
            return ResponseEntity.ok(repository.save(category));
        }
        else return ResponseEntity.badRequest().body("This category name is allready exist");
    }

    @Override
    public ResponseEntity<?> editCategory(Long id, EditCategoryRequest request) {
        Optional<Category> optional = repository.findById(id);
        if(optional.isPresent()){
            Category category = new Category();
            category = optional.get();
            category.setName(request.getName());
            category.setActive(request.isActive());
            category.setCode(request.getCode());
            repository.save(category);
            return ResponseEntity.ok("Successfully edited");
            }
        else return ResponseEntity.badRequest().body("Not edited. Category not found");
    }

    @Override
    public ResponseEntity<?> deleteCategory(Long id) {
        Optional<Category> optional = repository.findById(id);
        if(optional.isPresent()){
            repository.deleteById(id);
            return ResponseEntity.ok("Successfully deleted");
        }
        else return ResponseEntity.badRequest().body("Not deleted. Category not found");
    }
}
