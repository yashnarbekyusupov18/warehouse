package uz.yuyar.appwarehouse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.yuyar.appwarehouse.dto.ProductDto;
import uz.yuyar.appwarehouse.entity.Attachment;
import uz.yuyar.appwarehouse.entity.Category;
import uz.yuyar.appwarehouse.entity.Measurement;
import uz.yuyar.appwarehouse.entity.Product;
import uz.yuyar.appwarehouse.repository.AttachmentRepository;
import uz.yuyar.appwarehouse.repository.CategoryRepository;
import uz.yuyar.appwarehouse.repository.MeasurementRepository;
import uz.yuyar.appwarehouse.repository.ProductRepository;
import uz.yuyar.appwarehouse.service.ProductService;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    MeasurementRepository measurementRepository;
    @Override
    public ResponseEntity<?> addProduct(ProductDto request) {
        boolean exist = productRepository.existsByNameAndCategoryId(request.getName(), request.getCategoryId());
        if(exist){
            return ResponseEntity.badRequest().body("Bunday mahsulot mavjud");
        }
        else{
            Product product = new Product();
            product.setName(request.getName());


            Optional<Category> optional = categoryRepository.findById(request.getCategoryId());
            if(optional.isPresent()){
                product.setCategoryId(optional.get());
            }
            else{
                return ResponseEntity.badRequest().body("Bunday categoriya mavjud emas");
            }


            Optional<Attachment> optionalAttachment = attachmentRepository.findById(request.getPhotId());
            if(optionalAttachment.isPresent()){
                product.setPhoto_Id(optionalAttachment.get());
            }
            else{
                return ResponseEntity.badRequest().body("Bunday categoriya mavjud emas");
            }


            Optional<Measurement> optionalMeasurement = measurementRepository.findById(request.getMeasurementId());
            if(optionalMeasurement.isPresent()){
                product.setMeasurement_Id(optionalMeasurement.get());
            }
            else{
                return ResponseEntity.badRequest().body("Bunday categoriya mavjud emas");
            }


            product.setCode("1");


            return ResponseEntity.ok(productRepository.save(product));
        }
    }
}
