package uz.yuyar.appwarehouse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.yuyar.appwarehouse.entity.Attachment;
import uz.yuyar.appwarehouse.entity.AttachmentContent;
import uz.yuyar.appwarehouse.repository.AttachmentContentRepository;
import uz.yuyar.appwarehouse.repository.AttachmentRepository;
import uz.yuyar.appwarehouse.service.AttachmentService;

import java.io.IOException;
import java.util.Iterator;

@Service
public class AttachmentServiceImpl implements AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Override
    public ResponseEntity<?> uploadFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());
        if (file != null) {
            String originalFilename = file.getOriginalFilename();
            Long size = file.getSize();
            String contentType = file.getContentType();

            Attachment attachment = new Attachment();
            attachment.setFileOriginalname(originalFilename);
            attachment.setSize(size);
            attachment.setContentType(contentType);
            Attachment savedAttachment = attachmentRepository.save(attachment);


            AttachmentContent attachmentContent = new AttachmentContent();
            attachmentContent.setAsosiyContent(file.getBytes());
            attachmentContent.setAttachment(attachment);
            attachmentContentRepository.save(attachmentContent);
            return ResponseEntity.ok(savedAttachment.getId());
        }
        return ResponseEntity.badRequest().body("Fayl saqlanmadi");
    }
}
