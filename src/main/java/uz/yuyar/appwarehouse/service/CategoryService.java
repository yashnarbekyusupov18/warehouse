package uz.yuyar.appwarehouse.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.yuyar.appwarehouse.dto.CategoryDto;
import uz.yuyar.appwarehouse.dto.requests.AddCategoryRequest;
import uz.yuyar.appwarehouse.dto.requests.AddMeasurementRequest;
import uz.yuyar.appwarehouse.dto.requests.EditCategoryRequest;
import uz.yuyar.appwarehouse.dto.requests.EditMeasurementRequest;

@Service
public interface CategoryService {
    ResponseEntity<?> getAll();

    ResponseEntity<?> getById(Long id);

    ResponseEntity<?> addCategory(CategoryDto request);

    ResponseEntity<?> editCategory(Long id, EditCategoryRequest request);

    ResponseEntity<?> deleteCategory(Long id);
}
