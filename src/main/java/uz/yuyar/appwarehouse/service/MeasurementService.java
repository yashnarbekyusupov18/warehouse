package uz.yuyar.appwarehouse.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.yuyar.appwarehouse.dto.requests.AddMeasurementRequest;
import uz.yuyar.appwarehouse.dto.requests.EditMeasurementRequest;
import uz.yuyar.appwarehouse.dto.responces.AddMeasurementResponce;
import uz.yuyar.appwarehouse.entity.Measurement;

import java.util.List;

@Service
public interface MeasurementService {

    ResponseEntity<?> getAll();

    ResponseEntity<?> getById(Long id);

    ResponseEntity<?> addMeasurement(AddMeasurementRequest request);

    ResponseEntity<?> editMeasurement(Long id, EditMeasurementRequest request);

    ResponseEntity<?> deleteMeasurement(Long id);

}
