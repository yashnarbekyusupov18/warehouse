package uz.yuyar.appwarehouse.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.yuyar.appwarehouse.dto.ProductDto;

@Service
public interface ProductService {

    ResponseEntity<?> addProduct(ProductDto request);
}
