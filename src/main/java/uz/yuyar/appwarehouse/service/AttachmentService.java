package uz.yuyar.appwarehouse.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.yuyar.appwarehouse.dto.CategoryDto;
import uz.yuyar.appwarehouse.dto.requests.EditCategoryRequest;

import java.io.IOException;

@Service
public interface AttachmentService {
  ResponseEntity<?> uploadFile(MultipartHttpServletRequest request) throws IOException;
}
