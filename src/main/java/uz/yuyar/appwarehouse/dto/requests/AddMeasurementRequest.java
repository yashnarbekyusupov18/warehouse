package uz.yuyar.appwarehouse.dto.requests;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AddMeasurementRequest {
    private String name;
    private boolean active = true;
}
