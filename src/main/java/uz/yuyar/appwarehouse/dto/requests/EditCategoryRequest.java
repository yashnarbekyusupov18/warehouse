package uz.yuyar.appwarehouse.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EditCategoryRequest {
    private String name;
    private boolean active;
    private String code;
}
