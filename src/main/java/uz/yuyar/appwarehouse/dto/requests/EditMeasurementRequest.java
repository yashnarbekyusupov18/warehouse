package uz.yuyar.appwarehouse.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EditMeasurementRequest {
    private String name;
    private boolean active;
}
