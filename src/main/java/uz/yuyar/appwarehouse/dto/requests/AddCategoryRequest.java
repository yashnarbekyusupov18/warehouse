package uz.yuyar.appwarehouse.dto.requests;

import lombok.*;
import uz.yuyar.appwarehouse.entity.Attachment;
import uz.yuyar.appwarehouse.entity.Category;
import uz.yuyar.appwarehouse.entity.Measurement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AddCategoryRequest {
    private String name;

    private boolean active;

    private Category parentCategory;

    private Attachment photo;

    private String code;

    private Measurement measurement;


}
