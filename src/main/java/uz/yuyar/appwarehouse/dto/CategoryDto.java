package uz.yuyar.appwarehouse.dto;

import lombok.Data;

@Data
public class CategoryDto {
    private String name;
    private Long parentCategoryId;

}
