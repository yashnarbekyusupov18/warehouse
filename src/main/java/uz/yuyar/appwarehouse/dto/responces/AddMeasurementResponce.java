package uz.yuyar.appwarehouse.dto.responces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddMeasurementResponce {
    private String message;
    private boolean success;
}
