package uz.yuyar.appwarehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.yuyar.appwarehouse.controller.ProductController;
import uz.yuyar.appwarehouse.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
    boolean existsByNameAndCategoryId(String name, Long category_id);
}
