package uz.yuyar.appwarehouse.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.yuyar.appwarehouse.dto.CategoryDto;
import uz.yuyar.appwarehouse.dto.requests.AddCategoryRequest;
import uz.yuyar.appwarehouse.dto.requests.AddMeasurementRequest;
import uz.yuyar.appwarehouse.dto.requests.EditCategoryRequest;
import uz.yuyar.appwarehouse.dto.requests.EditMeasurementRequest;
import uz.yuyar.appwarehouse.repository.CategoryRepository;
import uz.yuyar.appwarehouse.service.CategoryService;

@AllArgsConstructor
@RestController
@RequestMapping("Category")
public class CategoryController {

    private final CategoryService service;

    public ResponseEntity<?> getAll() {
        return service.getAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody CategoryDto request) {
        return service.addCategory(request);
    }

    @PutMapping("{id}")
    public ResponseEntity<?> edit(@PathVariable Long id, @RequestBody EditCategoryRequest request) {
        return service.editCategory(id, request);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return service.deleteCategory(id);
    }
}
