package uz.yuyar.appwarehouse.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.yuyar.appwarehouse.service.AttachmentService;

import java.io.IOException;

@AllArgsConstructor
@RestController
@RequestMapping("/attachment")
public class AttachmentController {


    private final AttachmentService service;

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(MultipartHttpServletRequest request) throws IOException {
        return service.uploadFile(request);
    }
}

