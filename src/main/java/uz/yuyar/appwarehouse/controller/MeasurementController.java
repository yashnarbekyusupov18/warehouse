package uz.yuyar.appwarehouse.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.yuyar.appwarehouse.dto.requests.AddMeasurementRequest;
import uz.yuyar.appwarehouse.dto.requests.EditMeasurementRequest;
import uz.yuyar.appwarehouse.service.MeasurementService;

@RestController
@AllArgsConstructor
@RequestMapping("Measurement")
public class MeasurementController {


    private final MeasurementService measurementService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return measurementService.getAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return measurementService.getById(id);
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody AddMeasurementRequest request) {
        return measurementService.addMeasurement(request);
    }

    @PutMapping("{id}")
    public ResponseEntity<?> edit(@PathVariable Long id, @RequestBody EditMeasurementRequest request) {
        return measurementService.editMeasurement(id, request);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return measurementService.deleteMeasurement(id);
    }
}
